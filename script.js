// craete DOM object
let testBlank = {
  // create h1 
  h1 : function() {

    let header = document.createElement( 'h1' );
    header.innerHTML = 'Тест по программированию';
    
    document.body.appendChild( header );
  },

  //create form for POST answer
  testForm : function() {

    let testingForm = document.createElement( 'form' );
    testingForm.id = 'id__test__form';
    testingForm.setAttribute('action', 'submit');
    testingForm.setAttribute('method', 'POST');
    
    document.body.appendChild( testingForm );
  },

  // create test blank
  blankForm : function() {

    let blank = document.createElement( 'ol' );
    
    //get node testingForm
    pTestForm = document.getElementById( 'id__test__form' );
    pTestForm.appendChild( blank );
    
    //cycle for create list li -> ul -> li
    //start at 0 for avoid 0 number, in output
    for (let i = 1; i < 4; i++) {
      
      let mainQuestion = document.createElement( 'li' );
      mainQuestion.innerHTML = ( 'Вопрос №' + i );
      mainQuestion.className = 'mainq__li'; 
        
      let subQuestion = document.createElement( 'ul' );
      subQuestion.className = 'subq__ul';
      
      //get correct path to node mainQuestion
      let pMainQuestion = mainQuestion;

      blank.appendChild( mainQuestion );
      pMainQuestion.appendChild( subQuestion);

      for ( let j = 1; j < 4; j++ ) {

        let radio = document.createElement( 'INPUT' );
        radio.setAttribute( 'type', 'radio' );
        radio.setAttribute( 'name', 'q'+ i );
        radio.value = 'q_' + i + '_a_' + j ; 

        let container = document.createElement( 'div' );
        container.className = 'div__question';

        let answer = document.createElement( 'li' );
        answer.innerHTML = ( 'Вариант ответа №' + j );

        subQuestion.appendChild( container );
        container.appendChild( radio );
        container.appendChild( answer );
      }
    }
  },
  
  //create button
  button : function() {
    
    let button = document.createElement( 'button' );
    button.innerHTML = 'Проверить мои результаты';
    
    pTestForm = document.getElementById( 'id__test__form' );
    pTestForm.appendChild( button );
  }
}

testBlank.h1()
testBlank.testForm();
testBlank.blankForm();
testBlank.button();